################################################################################
## WRITE values
################################################################################

record(ao, "$(P)$(R)Cur-S")
{
    field(DESC, "Current control (in V reg mode)")
    field(DTYP, "stream")
    field(EGU,  "A")
    field(OUT,  "@a2720.proto setCurrent($(P)$(R)) $(PORT)")
    field(PREC, "5")
    field(VAL,  "0.0")
}

record(bo, "$(P)$(R)Rst")
{
    field(DESC, "Reset status register/clear faults")
    field(DTYP, "stream")
    field(OUT,  "@a2720.proto resetStatus($(P)$(R)) $(PORT)")
    field(FLNK, "$(P)$(R)#CalcCommand2 PP")
}

record(bo, "$(P)$(R)Rmp-S")
{
    field(DESC, "Enables/disables ramping to setpoint")
    field(ZNAM, "Off")
    field(ONAM, "On")
    field(VAL,  "1")
    field(PINI, "YES")
}

################################################################################
## ON/OFF commands
################################################################################
record(bo, "$(P)$(R)Pwr-S")
{
    field(DESC, "Turn supply off/on")
    field(ZNAM, "Off")
    field(ONAM, "On")
    field(VAL, "0")
    field(FLNK, "$(P)$(R)#PwrOffCalc")
}

# Nothing to check for ON command. Forward to ForcedPwr-S
record(calcout, "$(P)$(R)#PwrOnCalc") {
    field(DESC, "Forward ON cmd")
    field(CALC, "A=1?1:0")
    field(INPA, "$(P)$(R)Pwr-S CPP")
    field(OOPT, "Transition To Non-zero")
    field(OUT,  "$(P)$(R)ForcedPwr-S PP")
}

# First check if the current is set. If not set current=0 before turning off
record(calcout, "$(P)$(R)#PwrOffCalc") {
    field(DESC, "Forward OFF cmd")
    field(CALC, "(A=0)&&(B=1)?1:0")
    field(INPA, "$(P)$(R)Pwr-S NPP")
    field(INPB, "$(P)$(R)Status-R NPP")
    field(OOPT, "Transition To Non-zero")
    field(PINI, "NO")
    field(OUT,  "$(P)$(R)#StartCounter.PROC")
}


record(bo, "$(P)$(R)ForcedPwr-S")
{
    field(DESC, "Turn supply off/on")
    field(DTYP, "stream")
    field(OUT,  "@a2720.proto setEnable($(P)$(R)) $(PORT)")
    field(ZNAM, "Off")
    field(ONAM, "On")
}

############################################################
# Mechanism to set current to zero then turn OFF
############################################################1

record(seq, "$(P)$(R)#StartCounter") {
    field(DESC, "Start off cmd counter")
    field(SELM, "All")

    # Set output current to ZERO
    field(DOL1, "0.0")
    field(LNK1, "$(P)$(R)Cur-S PP")

    # Enable 5 seconds counter
    field(DOL2, "0")
    field(LNK2, "$(P)$(R)#OffCmdCounter.DISA")

    # Reset 5 seconds counter
    field(DOL3, "0")
    field(LNK3, "$(P)$(R)#OffCmdCounter")

    # Enable calc record
    field(DOL4, "0")
    field(LNK4, "$(P)$(R)#TurnOffCalc.DISA")

}

record(calc, "$(P)$(R)#OffCmdCounter") {
    field(DESC, "5 seconds counter")
    field(DISA, "1")
    field(INPA, "$(P)$(R)#OffCmdCounter")
    field(CALC, "A+1")
    field(SCAN, "1 second")
}

# If PS is ON and current is near zero, turn OFF
record(calcout, "$(P)$(R)#TurnOffCalc") {
    field(DESC, "Turn off after 5 seconds")
    field(DISA, "1")

    field(CALC, "((A==5)||(B<=1.0))&&(C=1) ? 1 : 0")
    field(INPA, "$(P)$(R)#OffCmdCounter CPP")
    field(INPB, "$(P)$(R)Cur-R NPP")
    field(INPC, "$(P)$(R)Status-R NPP")

    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#OffCmd.PROC")
}

record(seq, "$(P)$(R)#OffCmd") {
    field(DESC, "Start off cmd counter")
    field(SELM, "All")

    # Turn off (forced) the power supply
    field(DOL1, "0")
    field(LNK1, "$(P)$(R)ForcedPwr-S PP")

    # Disable calc record
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#TurnOffCalc.DISA")

    # Disable 5 seconds counter
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)#OffCmdCounter.DISA")

    # Reset 5 seconds counter
    field(DOL4, "0")
    field(LNK4, "$(P)$(R)#OffCmdCounter")

}

###############################################################################
## CALCULATED values
################################################################################
# not used anywhere I guess. Maybe we can delete this section?

## Polarity
record(calc, "${P}${R}CurrentPosPolarity")
{
    field(DESC, "Current Positive Polarity")
    field(CALC, "A>0.1")
    field(INPA, "${P}${R}Cur-R CPP")
}


## Polarity
record(calc, "${P}${R}CurrentNegPolarity")
{
    field(DESC, "Current Positive Polarity")
    field(CALC, "A<-0.1")
    field(INPA, "${P}${R}Cur-R CPP")
}

################################################################################
## READ values
################################################################################

record(ai, "$(P)$(R)Vol-R")
{
    field(DESC, "Output voltage")
    field(DTYP, "stream")
    field(EGU,  "V")
    field(SCAN, "1 second")
    field(INP,  "@a2720.proto getOutputVoltage $(PORT)")
    field(PREC, "5")
}

record(ai, "$(P)$(R)Cur-R")
{
    field(DESC, "Output current")
    field(DTYP, "stream")
    field(EGU,  "A")
    field(SCAN, ".2 second")
    field(INP,  "@a2720.proto getOutputCurrent $(PORT)")
    field(PREC, "5")
}

record(ai, "$(P)$(R)SlewRateI-R")
{
    field(DESC, "Current Ramp Slew Rate")
    field(DTYP, "stream")
    field(EGU,  "A/s")
    field(SCAN, "1 second")
    field(INP,  "@a2720.proto getSlewRateI $(PORT)")
    field(PREC, "5")
}

record(ao, "$(P)$(R)SlewRateI-S")
{
    field(DESC, "Set current Ramp Slew Rate")
    field(DTYP, "stream")
    field(EGU,  "A/s")
    field(OUT,  "@a2720.proto setSlewRateI($(P)$(R)) $(PORT)")
    field(PREC, "5")
}

record(ai, "$(P)$(R)DCLnkVol-R")
{
    field(DESC, "DC Link voltage")
    field(DTYP, "stream")
    field(EGU,  "V")
    field(SCAN, "1 second")
    field(INP,  "@a2720.proto getDCLinkVoltage $(PORT)")
    field(PREC, "2")
}

record(ai, "$(P)$(R)HeatTmp-R")
{
    field(DESC, "Heatsink temperature")
    field(DTYP, "stream")
    field(EGU,  "deg C")
    field(SCAN, "1 second")
    field(INP,  "@a2720.proto getTemperature $(PORT)")
    field(PREC, "2")
}

record(mbbiDirect, "$(P)$(R)Sts-R")
{
    field(DESC, "Device status (MSB)")
    field(DTYP, "stream")
    field(SCAN, "1 second")
    field(INP,  "@a2720.proto getStatus $(PORT)")
}

# record(mbbiDirect, "$(P)$(R)StsLSB-R")
# {
#     field(DESC, "Device status (LSB)")
#     field(DTYP, "stream")
#     field(SCAN, "1 second")
#     field(INP,  "@a2720.proto getStatusLSB $(PORT)")
# }

record (stringin, "$(P)$(R)Model-R")
{
    field(DESC, "Model")
    field(DTYP, "stream")
    field(INP,  "@a2720.proto getModel($(P)$(R)FWVer-R) $(PORT)")
}
record (stringin, "$(P)$(R)FWVer-R")
{
    field(DESC, "Firmware Version")
}


############################################################
# Status Feedback
############################################################
record(calc, "$(P)$(R)#Status-Rev-calc") {
    field(DESC, "Power Converter Status reversed Calc")
    field(INPA, "$(P)$(R)Sts-R.B0 CPP")
    field(CALC, "A ? 0 : 1")
}

record(bi, "$(P)$(R)Status-R") {
    field(DESC, "Power Converter Status (ON/OFF)")
    field(VAL,  "0")
    field(ZNAM, "OFF")  field(ZSV, "NO_ALARM")
    field(ONAM, "ON")   field(OSV, "NO_ALARM")
    field(INP,  "$(P)$(R)#Status-Rev-calc CPP")
}

# When module is OFF because of fault, force ON/OFF command to OFF
record(calcout, "$(P)$(R)#CalcCommand1") {
    field(DESC, "Force OFF setpoint when mod is OFF")
    field(CALC, "((A = 0) && (B = 1)) ? 0 : 1")
    field(VAL,  "0")
    field(INPA, "$(P)$(R)Status-R CPP")
    field(INPB, "$(P)$(R)Fault-R NPP")
    field(OOPT, "Transition To Zero")
    field(OUT,  "$(P)$(R)ForcedPwr-S PP")
}

record(calcout, "$(P)$(R)#CalcCommand2") {
    field(DESC, "Force OFF setpoint when reset fault")
    field(CALC, "B = 1 ? 0 : 1")
    field(VAL,  "0")
    field(INPA, "$(P)$(R)Rst NPP")
    field(INPB, "$(P)$(R)Fault-R NPP")
    field(OOPT, "When Zero")
    field(OUT,  "$(P)$(R)ForcedPwr-S PP")
    #field(FLNK, "$(P)$(R)#CalcCommand4")
}

# When module is OFF because of fault, force current SP to zero
record(calcout, "$(P)$(R)#CalcCommand3") {
    field(DESC, "reset current setpoint when mod is OFF")
    field(CALC, "((A = 0) && (B = 1)) ? 0 : 1")
    field(VAL,  "0")
    field(INPA, "$(P)$(R)Status-R CPP")
    field(INPB, "$(P)$(R)Fault-R NPP")
    field(OOPT, "Transition To Zero")
    field(OUT,  "$(P)$(R)Cur-S PP")
}

record(calcout, "$(P)$(R)#CalcCommand4") {
    field(DESC, "reset current setpoint when reset fault")
    field(CALC, "B = 1 ? 0 : 1")
    field(VAL,  "0")
    field(INPA, "$(P)$(R)Rst NPP")
    field(INPB, "$(P)$(R)Fault-R NPP")
    field(OOPT, "When Zero")
    field(OUT,  "$(P)$(R)Cur-S PP")
}

