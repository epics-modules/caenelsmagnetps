program program_snl
 /*option +r;*/
option +r; /*Make re-entrant*/

%% #include <stdlib.h>
%% #include <stddef.h>
%% #include <string.h>
%% #include <ctype.h>
%% #include <stdio.h>
%% #include <math.h>
%% #include <epicsString.h>
%% #include <epicsStdio.h>

int j;
int m;
int n;
int proc_array;
int proc_default;
int size_array;
int ind;
int set_state;
int state_val;
int erase;
int start_idx;
int end_idx;
int cycle_n;
int i_cycle;
int nord;
int ps_status;
int ps_fault;

float setpoint;
float time_default[50] = {1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
       1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
       1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.};

float setpoint_default[50] = {0,10,20,30,40,50,60,70,80,90,100,
                            110,120,130,140,150,160,170,180,190,200,
                            210,220,230,240,250,240,230,220,210,200,
                            190,180,170,160,150,140,130,120,110,100,
                            90,80,70,60,50,40,30,20,10};
float array[500];
float time_array[500];
int message;
int lmessage;
int limit_changed=0;
int array_def=1;

assign ind          "{P}{R}CurrentIndex-R";
assign proc_array   "{P}{R}LoadWaveform.RVAL";
assign proc_default  "{P}{R}LoadDefault.RVAL";
assign set_state    "{P}{R}CyclingModeStatus-S";
assign state_val    "{P}{R}CyclingModeStatus-S.RVAL";
assign setpoint     "{P}{R}SetPoint";
assign array        "{P}{R}Array";
assign time_array   "{P}{R}Time";
assign erase        "{P}{R}EraseArray-S";
assign cycle_n      "{P}{R}CycleN-S";
assign i_cycle      "{P}{R}CurrentCycle-R";
assign end_idx      "{P}{R}EndIndex-S";
assign start_idx    "{P}{R}StartIndex-S";
assign nord         "{P}{R}Array.NORD";
assign message      "{P}{R}Status-R";
assign lmessage     "{P}{R}LoadStatus-R";
assign ps_status    "{P}{R}Status-Fwd";
assign ps_fault     "{P}{R}Fault-Fwd";

monitor end_idx;
monitor start_idx;
monitor proc_array;
monitor proc_default;
monitor state_val;
monitor erase;
monitor ps_status;
monitor ps_fault;

ss waveform {
    state init {
        when() {
            pvGet(start_idx);
            ind = start_idx;
            ind = 0;
            m = 0;
            i_cycle = 1;
            } state delaymode
    }
    state delaymode {
        // Control is changed to Start or Pause
        when(state_val) {
            pvGet(cycle_n);
            } state monitorchanges
        // Low limit is changed
        when(start_idx) {
            if (m != start_idx){
                ind = start_idx;
                m = start_idx;}
            } state delaymode
        // High limit is changed
        when(end_idx) {
            if (n != end_idx){
                pvGet(nord);
                if (end_idx<=nord){
                    size_array = end_idx;
                    ind = start_idx;
                } else {
                    size_array = nord;
                    }
                n = end_idx;}
            } state delaymode
        // Stopstate
        when(state_val == 0) {
            if (m){
                ind = start_idx;
            } else {
                ind = 0;
            }
            message = 0;
            pvPut(message);
        } state stop
    }
    state monitorchanges {
        // Running
        when((state_val == 1) && (ps_status == 1) && (ps_fault == 0)) {
            pvPut(ind);
            setpoint = array[ind];
            pvPut(setpoint);
            message = 1;
            pvPut(message);
            } state run

        // Prevent running when PS is NOK
        when((state_val == 1) && ((ps_status == 0) || (ps_fault == 1))) {
            } state cancel_start

        // Pause cyle
        when(state_val == 2) {
            message = 2;
            pvPut(message);
            } state stop
    }

    state cancel_start {
        when () {
            message = 3;
            pvPut(message);
            set_state = 0;
            pvPut(set_state, SYNC);
        } state monitorchanges       
    }

    state forced_stop {
        when () {
            message = 3;
            pvPut(message);
        } state stop       
    }

    // Wait interval
    state run {
        when ((ps_status == 0) || (ps_fault == 1)) {  
        } state forced_stop

        when(delay(time_array[ind])) {
            } state setval
    }
    state setval {
        // Next index
        when(ind<(size_array-1)) {
            ind +=1;
            } state delaymode
        // Set for next icycle
        when((ind>=(size_array-1))&&(i_cycle<(cycle_n))) {
            i_cycle +=1;
            pvPut(i_cycle);
            pvGet(start_idx);
            pvGet(end_idx);
            ind = start_idx;
            if (end_idx<=size_array && end_idx!=0){
                size_array = end_idx;
            }
            } state delaymode
        // Finish cicles
        when((ind>=(size_array-1))&&(i_cycle==(cycle_n))) {
            set_state = 0;
            i_cycle = 1;
            pvPut(set_state);
            pvGet(start_idx);
            pvGet(end_idx);
            pvGet(cycle_n);
            ind = start_idx;
            message = 0;
            pvPut(message);
            if (end_idx<=size_array && end_idx!=0){
                size_array = end_idx;
            }
            } state stop
        
        when ((ps_status == 0) || (ps_fault == 1)) {  
        } state forced_stop

    }
    state stop {
        when(delay(0.1)) {
            } state delaymode
    }
}

ss fillarray {
     state init {
        when() {
            } state loaddefault
     }
    state MonitorChanges {
        when(proc_array) {
            } state loadapv
        when(proc_default) {
            } state loaddefault
        when(end_idx) {
            limit_changed=1;
            } state set_lastval 
         when(limit_changed) {
            } state set_lastval 
    }

    state loaddefault {
        when() {
            memcpy(array, setpoint_default, sizeof setpoint_default);
            memcpy(time_array, time_default, sizeof time_default);
            printf("Default array loaded %f\n",array[0]);
            lmessage = 1;
            pvPut(array);
            pvPut(time_array);
            ind = 0;
            size_array = sizeof(setpoint_default)/sizeof(float);
            pvGet(nord);
            size_array = nord;
            proc_default = 0;
            array_def=0;
            pvPut(lmessage);
            } state set_lastval
    }
    state set_lastval {
        when(end_idx==0) {
            if (array_def==0){
            pvGet(nord);
            size_array = nord;
            }else{
                size_array = sizeof(setpoint_default)/sizeof(float);
                }
            limit_changed=0;
            } state MonitorChanges
        when(end_idx!=0) {
            if (end_idx<=size_array){
                size_array = end_idx;
            }else{
                size_array = sizeof(setpoint_default)/sizeof(float);
                }

            } state MonitorChanges
    }
    state loadapv {
        when() {
            pvGet(array);
            pvGet(time_array);
            printf("PV loaded %f\n",array[0]);
            lmessage = 2;
            pvPut(lmessage);
            pvGet(nord);
            size_array = nord;
            proc_array = 0;
            array_def = 1;
            } state MonitorChanges
    }
}

ss erase_array  {
     state montiorerase {
        when(erase) {
            for ( j = 0; j <sizeof(array)/sizeof(float); j++ ) {
                array[ j ] = 0; /* set element at location i to i + 100 */
            }
            for ( j = 0; j <sizeof(time)/sizeof(float); j++ ) {
                time_array[ j ] = 1; /* set element at location i to i + 100 */
            }
            lmessage = 0;
            pvPut(array);
            pvPut(time_array);
            pvPut(lmessage);
            } state montiorerase
     }
}
